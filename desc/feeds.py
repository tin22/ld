from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords
from .models import Thought


class Last_thoughts(Feed):
    title = 'My thoughts'
    link = '/daily/'
    description = 'Last thoughts in the days.'

    def items(self):
        return Thought.objects.order_by('-timestamp')[:10]

    def item_title(self, item):
        return str(item.timestamp)

    def item_description(self, item):
        return str(item.record)

    def item_link(self, item):
        return 'http://lifedesc:8000/last/'