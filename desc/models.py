from datetime import datetime

from django.db import models
from django.urls import reverse
from django.utils import timezone


class Daily(models.Model):
    """ Main daily indicators
    """
    date = models.DateField('date')
    push_ups = models.IntegerField(default=0)
    squates = models.IntegerField(default=0)  # Приседания
    weight = models.DecimalField(decimal_places=1, max_digits=3, default=0.0)
    steps = models.IntegerField(default=0)
    sleeptime = models.DurationField(default='00:00:00')
    deep_sleeptime = models.DurationField(default='00:00:00')
    temperature = models.DecimalField(decimal_places=1, max_digits=3, default=35.0)

    class Meta:
        db_table = 'daily'
        # managed = False
        verbose_name_plural = 'Dailies'

    def __str__(self):
        daily = [f'{self.date}', f'{self.weight}', f'({self.bmi()})', f'{self.push_ups}', f'{self.squates}',
                 f'{self.steps}', f'{self.sleeptime}', f'{self.deep_sleeptime}', f'{self.temperature}']
        return ' '.join(daily)

    def get_absolute_url(self):
        return f'/day/{self.pk}/'

    def bmi(self):
        """ Body mass index
        """
        return f'{round(float(self.weight) / 1.85 ** 2, 2)}'

    def dss(self):
        """ Deep sleep share
        """
        dss = self.deep_sleeptime.total_seconds() / self.sleeptime.total_seconds() * 100 if self.sleeptime.total_seconds() != 0 else 0
        return f'{round(dss, 1)}%'


class Tag(models.Model):
    """" Tags for thoughts
    """
    tag = models.CharField(max_length=32, blank=True, null=True)  # , unique=True)

    # note = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        db_table = 'tags'
        ordering = ('id',)

    def __str__(self):
        return self.tag


class Thought(models.Model):
    """ Thoughts of the day
    """
    timestamp = models.DateTimeField(default=timezone.now)
    record = models.TextField(blank=True, null=True)
    daily = models.ForeignKey(Daily, on_delete=models.DO_NOTHING, related_name='thoughts')
    tags = models.ManyToManyField(Tag, related_name='tags')

    class Meta:
        # managed = False
        db_table = 'thoughts'
        ordering = ('timestamp',)

    def __str__(self):
        # return str(self.timestamp.time) + ', ' + str(self.record)
        return f"{datetime.time(self.timestamp).strftime('%H:%M')} {self.record}"
        # words = self.record.split(' ')[:10]
        # return self.timestamp + ' ' + self.record

    def timestamp_brief(self):
        return f"{datetime.time(self.timestamp).strftime('%H:%M')}"

    def get_absolute_url(self):
        return reverse('thoughts:thought_detail', args=[self.pk])
